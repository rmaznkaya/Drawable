package com.drawle;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

import com.infragistics.controls.CategoryXAxis;
import com.infragistics.controls.ChartDataContext;
import com.infragistics.controls.ColumnSeries;
import com.infragistics.controls.CustomRenderTemplate;
import com.infragistics.controls.CustomRenderTemplateMeasureInfo;
import com.infragistics.controls.CustomRenderTemplateRenderInfo;
import com.infragistics.controls.DataChartView;
import com.infragistics.controls.NumericYAxis;
import com.infragistics.controls.Series;
import com.infragistics.graphics.Brush;
import com.infragistics.graphics.SolidColorBrush;

public class MarkerDrawable extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chardata);

        final TestData data = new TestData();

        DataChartView chart = new DataChartView(this);

        CategoryXAxis xAxis = new CategoryXAxis();
        xAxis.setDataSource(data);
        xAxis.setLabel("Label");

        NumericYAxis yAxis = new NumericYAxis();
        yAxis.setMinimumValue(0);
        yAxis.setMaximumValue(70);

        chart.addAxis(xAxis);
        chart.addAxis(yAxis);


        SolidColorBrush brush = new SolidColorBrush();
        brush.setColor(Color.RED);

        ColumnSeries series = new ColumnSeries();
        series.setXAxis(xAxis);
        series.setYAxis(yAxis);
        series.setDataSource(data);
        series.setBrush(brush);
        series.setMarkerOutline(brush);
        series.setOutline(brush);
        series.setValueMemberPath("Value");

        //series.setToolTip(new TooltipAdapter(this));

        final Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setTextSize(27);
        Typeface t = Typeface.create("Arial", Typeface.NORMAL);

        p.setTypeface(t);
        Rect bounds = new Rect();
        p.getTextBounds("M", 0, 1, bounds);
        final boolean[] b = {true};

        final float textHeight = bounds.height();

        series.setMarkerTemplate(new CustomRenderTemplate() {
            @Override
            public void measure(CustomRenderTemplateMeasureInfo info) {
                ChartDataContext context = (ChartDataContext)info.getData();
                TestDataItem tdi = (TestDataItem)context.getItem();
                double d = tdi.getValue();
                String str = String.valueOf(d);

                float width = p.measureText(str);
                float height = textHeight;

                info.setWidth(width);
                info.setHeight(height);
            }

            @Override
            public void render(CustomRenderTemplateRenderInfo info) {
                Canvas c = info.getContext();
                ChartDataContext context = (ChartDataContext)info.getData();
                Series s = context.getSeries();
                TestDataItem tdi = (TestDataItem)context.getItem();
                int d = (int) tdi.getValue();
                String str = String.valueOf(d);

               com.infragistics.graphics.Rect viewport = s.getSeriesViewer().getViewportRect();
                float left = (float)info.getXPosition() - (float)info.getAvailableWidth() / 2.0f;

              //  float reight =
                Log.d("Canvas Widh " ,c.getWidth()+"   Hight  "+c.getHeight());//1280   Hight  670
                float top = (float)(info.getYPosition() - (float)info.getAvailableHeight() / 2.0f) -50;
                top = top - p.getFontMetrics().top - p.getFontMetrics().descent;

                Log.d("GET Widh " ,info.getAvailableWidth()+"   Hight  "+info.getAvailableHeight());

                Log.d(" Top ",top+"  Left "+left);
                Paint    mGuidelinePaint = new Paint();
             mGuidelinePaint.setStyle(Paint.Style.STROKE);
              //  mGuidelinePaint.setColor(Color.BLUE);
           //     mGuidelinePaint.setStrokeWidth(60);
               // mGuidelinePaint.setAntiAlias(true);

        //c.drawPoint(left+30,top-15,mGuidelinePaint);

               // c.drawRoundRect(rectf,left,top,mGuidelinePaint);
                  //  c.drawRect(100, top, (c.getWidth()-(2*data.size()))/data.size(),40, mGuidelinePaint);
                 //   c.drawRect(rectf,mGuidelinePaint);
                c.drawText(str, left, top, p);
//                c.drawLine(left-50, top+50, left, top,
//                        mGuidelinePaint);


            }
        });

        chart.addSeries(series);

        LinearLayout layout = (LinearLayout)findViewById(R.id.layout);
        layout.addView((chart));
    }
}
