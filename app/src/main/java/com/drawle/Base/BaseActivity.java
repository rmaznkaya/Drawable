package com.drawle.Base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.drawle.R;
import com.drawle.Utils.CustomConnection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.SqlDateTypeAdapter;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class BaseActivity extends AppCompatActivity {

    public Gson fromGson;
    public  SharedPreferences sharedPref;
   public  String TokenId=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SqlDateTypeAdapter sqlAdapter = new SqlDateTypeAdapter();
        fromGson = new GsonBuilder().registerTypeAdapter(java.sql.Date.class, sqlAdapter)
                .setDateFormat("yyyy-MM-dd")
                .create();//
    }

    public void  StartJsonRequest(RequestHelpper helpper){

        CustomConnection connection=new CustomConnection();
        connection.setCustomConnectionListener(new CustomConnection.CustomConnectionListener() {
            @Override
            public void ConnectionDidFinish(ResultHelper result) {
                FinishJsonRequest(result);
            }
        });
        if (BaseActivity.this!=null)
            connection.Execute(BaseActivity.this,helpper);
    }

    public void FinishJsonRequest(ResultHelper resultHelper){

    }



    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    public  void showMessage(String msj){

        Toast.makeText(BaseActivity.this,msj,Toast.LENGTH_SHORT).show();
    }

}// son bir not dahah
