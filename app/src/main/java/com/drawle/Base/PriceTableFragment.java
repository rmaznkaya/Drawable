package com.drawle.Base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.drawle.R;
import com.drawle.Type.BranchContract;
import com.drawle.Type.BranchRequest;
import com.drawle.Type.PriceTableContract;
import com.drawle.Type.PriceTableRequest;
import com.drawle.Utils.ListviewAdapter;
import com.drawle.Utils.SpinnerAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class PriceTableFragment extends BaseActivity implements  View.OnClickListener {

    Spinner spinner_branch,spinner_branch2;
    Button btn_takePrice,btn_priceList;
    PriceTableRequest priceTableRequest;
    List<BranchContract> list_branch;
    EditText edit_price;
    ViewGroup layout_relative;
    List<PriceTableContract> priceTableContracts;
   ListView list_view;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.price_table);

        priceTableRequest=new PriceTableRequest();


        layout_relative=findViewById(R.id.layout_relative);
        list_view=findViewById(R.id.list_item);
        edit_price=findViewById(R.id.edit_price);
        spinner_branch=findViewById(R.id.spinner_branch);
        spinner_branch2=findViewById(R.id.spinner_branch2);
        btn_takePrice=findViewById(R.id.btn_take_price);
        btn_takePrice=findViewById(R.id.btn_take_price);
        btn_priceList=findViewById(R.id.btn_price_list);
        btn_takePrice.setOnClickListener(this);
        btn_priceList.setOnClickListener(this);
        RequestHelpper helpper=new RequestHelpper();
        BranchRequest request=new BranchRequest();
        request.MethodName="Select";
        request.IsActive=true;
        helpper.base=request;
        StartJsonRequest(helpper);

        spinner_branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BranchContract branchContract=list_branch.get(position);
                priceTableRequest.BranchId=branchContract.BranchId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Toast.makeText(this,"Lütfen Fiyatı sadece sayı oalrak girin!!!",Toast.LENGTH_SHORT).show();
        spinner_branch2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              BranchContract branchContract=list_branch.get(position);
                priceTableRequest.BranchId2=branchContract.BranchId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void FinishJsonRequest(ResultHelper result) {
        if (result.IsSuccess){
            JSONArray jsonArray = null;
            String className=result.Request.base.getClass().getSimpleName();
            if (className.equals(BranchRequest.class.getSimpleName())) {
                list_branch = new ArrayList<>();

                try {
                    jsonArray = result.ResultData.getJSONArray(result.Request.parseString);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        BranchContract contract = fromGson.fromJson(jsonArray.get(i).toString(), BranchContract.class);
                        list_branch.add(contract);
                    }

                    SpinnerAdapter adapter=new SpinnerAdapter(this,list_branch);
                    spinner_branch.setAdapter(adapter);
                    spinner_branch2.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if (className.equals(PriceTableRequest.class.getSimpleName())){
                if (result.Request.IsSpacila==145) {
                    priceTableRequest.BranchId = 0;
                    priceTableRequest.BranchId2 = 0;
                    edit_price.setText("");
                    showMessage("Fiyat Eklendi");
                }else {
                    try {
                            layout_relative.setVisibility(View.GONE);
                            list_view.setVisibility(View.VISIBLE);
                        priceTableContracts=new ArrayList<>();
                        jsonArray = result.ResultData.getJSONArray(result.Request.parseString);

                        for (int i=0;i<jsonArray.length();i++){
                        PriceTableContract contract = fromGson.fromJson(jsonArray.get(i).toString(), PriceTableContract.class);
                            priceTableContracts.add(contract);
                        }

                        ListviewAdapter listviewAdapter=new ListviewAdapter(this,priceTableContracts);
                        list_view.setAdapter(listviewAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



    void setRequest(){
        if (priceTableRequest.BranchId!=0 &&priceTableRequest.BranchId2!=0 &&edit_price.getText().toString().length()!=0) {
            RequestHelpper helpper = new RequestHelpper();
            helpper.IsSpacila=145;
            priceTableRequest.MethodName="Insert";
            priceTableRequest.IsActive = true;
            priceTableRequest.Price=Integer.parseInt(edit_price.getText().toString());
            helpper.base = priceTableRequest;
            StartJsonRequest(helpper);
        }else
            showMessage("şube Seçin ve Fiyat verin !!");

    }
    void selectRequest(){

            RequestHelpper helpper = new RequestHelpper();
            priceTableRequest.MethodName="Select";
            priceTableRequest.BranchId=-54;
            priceTableRequest.IsActive = true;
            helpper.base = priceTableRequest;
            StartJsonRequest(helpper);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btn_take_price:
                setRequest();

                break;

            case R.id.btn_price_list:
                selectRequest();
                break;
        }
    }
}
