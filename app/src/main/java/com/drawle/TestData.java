package com.drawle;

import java.util.ArrayList;
import java.util.Random;

public class TestData extends ArrayList<TestDataItem> {

    public TestData(){

        for(int i=1; i<10; i++){

            TestDataItem item = new TestDataItem();
            item.setLabel("Lbl " + ((java.lang.Integer)i).toString());
            Random r = new Random();
            int i1 = r.nextInt(2000 - 1990) + 50;
            item.setValue(i1);
            add(item);
        }

    }
}
