package com.drawle.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.drawle.R;
import com.drawle.Type.BranchContract;
import com.drawle.Type.PriceTableContract;

import java.util.List;

public class ListviewAdapter extends BaseAdapter {

    List<PriceTableContract> list;
    Context cnt;
    private LayoutInflater mInflater;

   public ListviewAdapter(Context context, List<PriceTableContract> list){

       this.cnt=context;
       this.list=list;
       mInflater = LayoutInflater.from(context);

   }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return  list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PriceTableContract rowItem = list.get(position);

        viewHolder holder ;
        View view = convertView;
        if (view==null) {

            holder = new viewHolder();

            view = mInflater.inflate(R.layout.list_layout, null);
            holder.txtTitle = view.findViewById(R.id.txt_spinner_item);
            holder.txtTitle2 = view.findViewById(R.id.txt_spinner_item2);
            holder.txtTitle3 = view.findViewById(R.id.txt_spinner_item3);
            view.setTag(holder);
        }else{
            holder = (viewHolder) view.getTag();
        }
        holder.txtTitle.setText(rowItem.BranchName);
        holder.txtTitle2.setText(rowItem.BranchName2);
        holder.txtTitle3.setText(rowItem.Price+" TL");

        return view;
    }





    private class viewHolder{
        TextView txtTitle,txtTitle2,txtTitle3;
        ImageView imageView;
    }
}
