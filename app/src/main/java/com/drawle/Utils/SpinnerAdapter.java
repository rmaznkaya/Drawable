package com.drawle.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.drawle.R;
import com.drawle.Type.BranchContract;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    List<BranchContract> list;
    Context cnt;
    private LayoutInflater mInflater;

   public SpinnerAdapter(Context context, List<BranchContract> list){

       this.cnt=context;
       this.list=list;
       mInflater = LayoutInflater.from(context);

   }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return  list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BranchContract rowItem = list.get(position);

        viewHolder holder ;
        View view = convertView;
        if (view==null) {

            holder = new viewHolder();

            view = mInflater.inflate(R.layout.spinner_layout, null);
            holder.txtTitle = view.findViewById(R.id.txt_spinner_item);
            view.setTag(holder);
        }else{
            holder = (viewHolder) view.getTag();
        }
        holder.txtTitle.setText(rowItem.BranchName);

        return view;
    }





    private class viewHolder{
        TextView txtTitle;
        ImageView imageView;
    }
}
