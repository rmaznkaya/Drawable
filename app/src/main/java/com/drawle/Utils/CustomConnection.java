package com.drawle.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;

import com.drawle.Base.RequestHelpper;
import com.drawle.Base.ResultHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by Ramazan on 29.09.2017.
 */

public class CustomConnection {

    ProgressDialog dialog;
   //public static String Url="http://192.168.2.30:19033/Lonca/api/Hafifyuk/";
     public static String Url="http://plasee.site/api/Hafifyuk/";
    public interface CustomConnectionListener{
        public void ConnectionDidFinish(ResultHelper result);
    }

    private CustomConnectionListener customConnectionListener;

    public CustomConnectionListener getCustomConnectionListener() {
        return customConnectionListener;
    }
    public void setCustomConnectionListener(CustomConnectionListener customConnectionListener) {
        this.customConnectionListener = customConnectionListener;
    }

    private RequestHelpper request;
    public void setRequest(RequestHelpper request) {
        this.request = request;
    }

    private Activity activitiy;
    public void setActivitiy(Activity activitiy) {
        this.activitiy = activitiy;
    }


    public void Execute(Activity _activity, RequestHelpper _request)
    {
        this.setRequest(_request);
        this.activitiy=_activity;
        if (request.base!=null)
            request.base.Name=request.base.getClass().getSimpleName();


      // request.base.AutKey= ApplicationContext.Instance.usUser.AutKey;

        if(isConn())
        {
            MakeRequestAsync task = new MakeRequestAsync();
            task.execute();
        }
        else
        {
            ShowAlertMessage("Aktif Internet Bağlantısı Bulunamadı !!!");
        }



    }

    private void ShowAlertMessage(String message){

        new AlertDialog.Builder(this.activitiy)
                .setMessage(message)
                .setNeutralButton("Tamam",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                dialog.cancel();
                            }
                        }).show();
    }
    public boolean isConn(){
        if (this.activitiy!=null) {
            ConnectivityManager connectivity = (ConnectivityManager) this.activitiy.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity.getActiveNetworkInfo() != null) {
                if (connectivity.getActiveNetworkInfo().isConnected())
                    return true;
            }
        }
        return false;
    }


    private class MakeRequestAsync extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute(){
            dialog = new ProgressDialog(activitiy);
            dialog.setMessage("Lütfen Bekleyin");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
         if (request.IsShowDialog)
            dialog.show();
        }

        @Override
        protected String doInBackground(String... urls ){

            try {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                JSONObject postDataParams = new JSONObject(gson.toJson(request.base));
             //   postDataParams.put("request", gson.toJson(request));

                JSONObject obj = new JSONObject();
                obj.put("Request",postDataParams.toString());
                obj.put("Id", 0);

                System.out.println(obj.toString());

                URL url = new URL(Url+request.base.getClass().getSimpleName()); // here is your URL path

                Log.d("URL  ",Url+request.base.getClass().getSimpleName());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(25000 /* milliseconds */);
                conn.setConnectTimeout(25000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(obj));

                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();
                }else {
                    Log.d("Connetion Error ",conn.getErrorStream().toString());
                }
            }
            catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
               return e.getMessage();
            }
            catch (JSONException e) {
                // TODO Auto-generated catch block
              return   e.getMessage();
            } catch (IOException e) {
                // TODO Auto-generated catch block
             return    e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result){

            if(dialog!=null)
            {
                dialog.dismiss();
            }
            ResultHelper resultHelper = new ResultHelper();
            resultHelper.Request=request;

            if (result!=null)
            {
                System.out.println(result);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);
                    if(jsonObject!=null)
                    {
                     //   JSONObject jSonResult= jsonObject.getJSONObject("SessionTimeOut");


                        Boolean SessionTimeOut=jsonObject.getBoolean("SessionTimeOut");
//                        if(SessionTimeOut){
//
//                            new AlertDialog.Builder(activitiy)
//                                    .setMessage("Session Süresi Doldu Tekrar Giriş Yapın")
//                                    .setNeutralButton("Tamam",
//                                            new DialogInterface.OnClickListener() {
//
//                                                public void onClick(DialogInterface dialog,int which) {
//                                                    dialog.cancel();
//                                                   SharedPreferences sharedPref = activitiy.getSharedPreferences(StatickValue.CustomCallDetail,Context.MODE_PRIVATE);
//                                                    Intent intent = new Intent( activitiy, MainActivity.class );
//                                                    intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
//                                                    SharedPreferences.Editor editor=sharedPref.edit();
//                                                    editor.putString(StatickValue.TokenId,null);
//                                                    editor.putBoolean(StatickValue.usLogin,false);
//                                                    editor.putBoolean(StatickValue.Login,false);
//                                                    editor.putString(StatickValue.userDetail,null);
//                                                    editor.commit();
//                                                    activitiy.startActivity( intent );
//                                                }
//                                            }).show();
//
//                            return;
//                        }

                        Boolean IsSuccess=jsonObject.getBoolean("IsSuccess");
                        if(IsSuccess)
                        {
                            resultHelper.IsSuccess=true;

                            resultHelper.ResultData=jsonObject;
                            //resultHelper.Description=jsonObject.getString("Description");
                        }
                        else
                        {
                            resultHelper.Description=jsonObject.getString("Description");
                        }
                    //    resultHelper.Request.IsResult=true;
                    }

                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    resultHelper.Description=e1.getMessage();
                }
            }
            else
            {
                resultHelper.Description="Result is Null";

            }

            if (customConnectionListener!=null)
            {
                customConnectionListener.ConnectionDidFinish(resultHelper);
            }
        }

    }
    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


}

