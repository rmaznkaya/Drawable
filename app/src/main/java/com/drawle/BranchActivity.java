package com.drawle;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.drawle.Base.BaseActivity;
import com.drawle.Base.PriceTableFragment;
import com.drawle.Base.RequestHelpper;
import com.drawle.Base.ResultHelper;
import com.drawle.Type.BranchRequest;

public class BranchActivity extends BaseActivity {


    Button button_add_barnch,btn_PriceTabele;
    TextView txt_statutype;
    SeekBar seekBar;
    EditText edit_branchName;
  //  private FusedLocationProviderClient mFusedLocationProviderClient;;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.branch);

        button_add_barnch=findViewById(R.id.btn_add_branch);
        btn_PriceTabele=findViewById(R.id.btn_PriceTabele);
        txt_statutype=findViewById(R.id.txt_statutype);
        seekBar=findViewById(R.id.seekbar);
        edit_branchName=findViewById(R.id.edit_branchname);
        seekBar.setMax(2);

        button_add_barnch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edit_branchName.getText().toString().length()>3 && seekBar.getProgress()!=0) {
                    RequestHelpper helpper = new RequestHelpper();
                    BranchRequest request = new BranchRequest();
                    request.MethodName = "Insert";
                    request.BranchName = edit_branchName.getText().toString();
                    request.StatuType = seekBar.getProgress();
                    request.BranchCountyId=0;
                    request.IsActive=true;
                    helpper.base=request;
                    StartJsonRequest(helpper);
                }else {
                    showMessage("Bayi isimi ve Statu Tipini (1,2) seçin !!");

                }




            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    txt_statutype.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
//dsed
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn_PriceTabele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iIntent=new Intent(BranchActivity.this, PriceTableFragment.class);
                startActivity(iIntent);
            }
        });
    }

    @Override
    public void FinishJsonRequest(ResultHelper resultHelper) {
        super.FinishJsonRequest(resultHelper);
        if (resultHelper.IsSuccess) {
            seekBar.setProgress(0);
            edit_branchName.setText("");
            Toast.makeText(this,"Bayi Eklendi",Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this,"Lütfen Tekrar Deneyin!",Toast.LENGTH_SHORT).show();
    }

//    private void getDeviceLocation(){
//        Log.d(" ", "getDeviceLocation: getting the devices current location");
//
//        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
//
//        try{
//            if(true){
//
//                final Task location = mFusedLocationProviderClient.getLastLocation();
//                location.addOnCompleteListener(new OnCompleteListener() {
//                    @Override
//                    public void onComplete(@NonNull Task task) {
//                        if(task.isSuccessful()){
//                            Log.d(" ", "onComplete: found location!");
//                            Location currentLocation = (Location) task.getResult();
//
//
//                        }else{
//                            Log.d(" ", "onComplete: current location is null");
//                        }
//                    }
//                });
//            }
//        }catch (SecurityException e){
//            Log.e("  ", "getDeviceLocation: SecurityException: " + e.getMessage() );
//        }
//    }
}
